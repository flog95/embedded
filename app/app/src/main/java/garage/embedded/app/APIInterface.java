package garage.embedded.app;

import android.telecom.Call;

import java.util.HashMap;

import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface APIInterface {
    @FormUrlEncoded
    @POST("api/")
    Call staticPagesApi(@FieldMap HashMap<String, String> requestBody);

}
