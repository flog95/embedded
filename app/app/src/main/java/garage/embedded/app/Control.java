package garage.embedded.app;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class Control extends AppCompatActivity implements SensorEventListener {

    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private TextView sensorText_z;
    private TextView sensorText_y;
    private TextView sensorText_x;
    private TextView state;
    private static final int QUEUE_SIZE = 20;
    private float accelCurr;
    private float accelLast;
    float accel;



    private boolean continueOp = false;
    private long lastUpdate = System.currentTimeMillis();
    private float last_x, last_y, last_z;
    private boolean open = false;
    private String s = "";
    private static boolean again = false;

    public boolean isExecuted = false;


    private Queue<Float> valuesQ = new PriorityQueue<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        accelCurr = SensorManager.GRAVITY_EARTH;
        accelLast = SensorManager.GRAVITY_EARTH;
        accel = 0.0f;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sensorText_z = (TextView) findViewById(R.id.sensor_z);
        sensorText_y = (TextView) findViewById(R.id.sensor_y);
        sensorText_x = (TextView) findViewById(R.id.sensor_x);
        state = (TextView) findViewById(R.id.state);

        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        String sensor_error = getResources().getString(R.string.error_no_sensor);

        if (senAccelerometer == null) {
            sensorText_z.setText(sensor_error);
        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_control, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            float z = event.values[2]*10;

            sensorText_z.setText(getResources().getString(R.string.accelerometer_z_1_2f, z));

            if (z > 160f) {
                Float max_z = z;
                long start = System.currentTimeMillis();

                while(System.currentTimeMillis() - start < 500) {
                    max_z = Math.max(event.values[2]*10, max_z);
                }

                float x = event.values[0]*10;
                float y = event.values[1]*10;
                z = event.values[2]*10;

                sensorText_z.setText(getResources().getString(R.string.accelerometer_z_1_2f, z));

                sensorText_y.setText(getResources().getString(R.string.accelerometer_y_1_2f, y));

                sensorText_x.setText(getResources().getString(R.string.accelerometer_x_1_2f, x));

                if (!open && max_z > 160f) {
                    s = "open";
                    state.setText(getResources().getString(R.string.state, s));
                    open = true;
                    new Compute().execute(s);
                    max_z = 0f;
                    //isExecuted = true;

                    //z = 0;

                } else if (open && max_z > 160f){
                    s = "close";
                    state.setText(getResources().getString(R.string.state, s));
                    open = false;
                    new Compute().execute(s);
                    isExecuted = false;
                    max_z = 0f;


                    //z = 0;
                }


            }
        }
    }

    public boolean openDoor(boolean opener) {
        return opener;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_UI);
        //new Compute().execute(s);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (senAccelerometer != null) {
            senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_UI);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        senSensorManager.unregisterListener(this);
    }

    private static class Compute extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            //again = true;
        }

        @Override
        protected String doInBackground(String... isOpen) {

            String s = isOpen[0];

            //if (again) {
                if (s.equals("open")) {
                    //state.setText(getResources().getString(R.string.state, "open"));
                    HttpURLConnection open = null;
                    try {
                        URL openUrl = new URL("http://192.168.4.1:5000/open");
                        open = (HttpURLConnection) openUrl.openConnection();
                        InputStream in = new BufferedInputStream(open.getInputStream());
                        in.read();
                        open.setRequestMethod("GET");
                        open.connect();
                        open.disconnect();
                        in.close();


                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    //state.setText(getResources().getString(R.string.state, "close"));
                    HttpURLConnection close = null;
                    try {
                        URL closeURL = new URL("http://192.168.4.1:5000/close");
                        close = (HttpURLConnection) closeURL.openConnection();
                        InputStream in = new BufferedInputStream(close.getInputStream());
                        in.read();
                        close.setRequestMethod("GET");
                        close.connect();
                        close.disconnect();
                        in.close();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            //} again = false;

            return s;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //again = false;
        }

    }

}
