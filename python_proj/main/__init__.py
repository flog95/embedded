import RPi.GPIO as GPIO
import time

from flask import Flask
from flask import jsonify


class DoorOpener:


    def __init__(self):
        self.servoPIN = 18
        self.frequency = 50
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.servoPIN, GPIO.OUT)
        self.p = GPIO.PWM(self.servoPIN, self.frequency)  # GPIO 18 als PWM mit 50Hz
        self.p.start(2.5)  # Initialisierung

    def openDoor(self):

        self.p.ChangeDutyCycle(2.5)
        time.sleep(0.1)
        self.p.ChangeDutyCycle(5)
        time.sleep(0.1)
        self.p.ChangeDutyCycle(7.5)
        time.sleep(0.1)
        self.p.ChangeDutyCycle(10)
        time.sleep(0.1)
        self.p.ChangeDutyCycle(11.0)

        time.sleep(0.1)
        #isDone = True

        #if (isDone):
        time.sleep(2)
        #isDone = False

    def closeDoor(self):
        self.p.ChangeDutyCycle(10)
        time.sleep(0.5)
        self.p.ChangeDutyCycle(7.5)
        time.sleep(0.5)
        self.p.ChangeDutyCycle(5)
        time.sleep(0.5)
        self.p.ChangeDutyCycle(2.5)
        time.sleep(0.5)


app = Flask(__name__)

@app.route('/open')
def open():
    myDoorOpener = DoorOpener()
    myDoorOpener.openDoor()
    return jsonify(result='Open')




@app.route('/close')
def close():
    myDoorOpener = DoorOpener()
    myDoorOpener.closeDoor()
    return jsonify(result='Close')

if __name__ == '__main__':
    app.run(debug=True,host="192.168.4.1")



